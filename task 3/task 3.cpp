﻿#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
int main(){
    srand(time(0));
    float arr[100] = {-11};
    int n, k;
    printf("Enter array size and shift: ");
    scanf("%d %d", &n, &k);
    if(n < k || n < 2 || n > 100 || k > 100 || k < 1){
        printf("Error: Wrong size of array or shift\n");
        return 0;
    }
    printf("arr = { ");
    for(int i = 0; i < n; i++){
        arr[i] = (float)(rand() % 201) / 10 - 10;
        printf("%4.1f ", arr[i]);
    }
    printf("}\n");
    for (int i = k; i <=n; i++)
        arr[i-k] = arr[i];
    for (int i = n - k; i < n; i++)
        arr[i] = 0;
    printf("arr = { ");
    for (int i = 0; i < n; i++) {
        printf("%4.1f ", arr[i]);
    }
}