﻿#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
int main() {
    srand(time(0));
    int n, arr[100] = {0}, lastLocalMaximumIndex = -1;
    printf("Enter size of array: ");
    scanf("%d", &n);
    if (n < 2 || n > 100){
        printf("Bad array size");
        return 0;
    }
    printf("arr = {");
    for (int i = 0; i < n; i++) {
        arr[i] = rand() % 201 - 100;
        printf("%d ", arr[i]);
    }        
    printf("}\n");
    for (int i = n-2; i > 0 && lastLocalMaximumIndex == -1; i--) {
        if (arr[i] > arr[i - 1] && arr[i] > arr[i + 1]) lastLocalMaximumIndex = i;
    }
    if (lastLocalMaximumIndex == -1)
        printf("Last local maximum doesnt exist\n");
    else
        printf("Last local maximum number is %d", lastLocalMaximumIndex);
    return 0;
}