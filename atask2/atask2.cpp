﻿#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
int main() {
    srand(time(0));
    float f[14] = {0};    
    printf("f = { ");
    for (int i = 0; i < 14; i++) {
        f[i] = (float)(rand() % 201) / 10 - 10;
        printf("%4.1f ", f[i]);
    }
    printf("}\n");
    for (int i = 0; i < 14; i++)
        for (int j = i + 1; j < 14; j++)
            if (f[i] < f[j]) {
                f[i] = f[i] + f[j];
                f[j] = f[i] - f[j];
                f[i] = f[i] - f[j];
            }
    printf("f = { ");
    for (int i = 0; i < 14; i++)
        printf("%4.1f ", f[i]);
    printf("}\n");
    return 0;
}