﻿#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
int main(){
    srand(time(0));
    int n, a[100] = {0}, b[100] = {0}, c[100] = {0};
    printf("Enter size of array: ");
    scanf("%d", &n);
    if (n < 1 || n > 100) {
        printf("Error: Bad array size\n");
        return 0;
    }
    printf("a={");
    for(int i = 0; i < n; i++){
        a[i] = rand() % 201 - 100;
        printf("%d ", a[i]);
    }
    printf("}\n");    
    printf("b={");
    for (int i = 0; i < n; i++) {
        b[i] = rand() % 201 - 100;
        printf("%d ", b[i]);
    }
    printf("}\n");        
    printf("c={");
    for (int i = 0; i < n; i++) {
        c[i] = a[i] > b[i] ? a[i] : b[i];
        printf("%d ", c[i]);
    }
    printf("}\n");
    return 0;
}