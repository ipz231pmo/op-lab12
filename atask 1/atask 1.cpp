﻿#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
int main() {
    srand(time(0));
    float l[11] = { 0 }, lout[11] = { 0 }; 
    int numberOfPositiveNumbers = 0, numberOfNegativeNumbers = 0;
    printf("l = { ");
    for (int i = 0; i < 11; i++) {
        l[i] = (float)(rand() % 201)/10 - 10;
        if (l[i] < 0) numberOfNegativeNumbers++;
        if (l[i] > 0) numberOfPositiveNumbers++;
        printf("%4.1f ", l[i]);
    }
    printf("}\n");
    int j = 0;
    for (int i = 0; i < 11; i++)
        if (l[i] > 0) {
            lout[j] = l[i];
            j++;
        }
    for (int i = 0; i < 11; i++)
        if (l[i] <= 0) {
            lout[j] = l[i];
            j++;
        }
    printf("l = { ");
    for (int i = 0; i < 11; i++)
        printf("%4.1f ", lout[i]);
    printf("}\n");
    printf("Count of Positive numbers: %d, Negative numbers: %d\n", numberOfPositiveNumbers, numberOfNegativeNumbers);
    return 0;
}